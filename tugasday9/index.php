<?php
require_once 'Animal.php';
require_once 'Frog.php';
require_once 'Ape.php';

//Class Animal
$sheep = new Animal("shaun");
echo "Name : ";
echo $sheep->name . "<br>"; 
echo "Legs : ";
echo $sheep->legs . "<br>";
echo "Cold Blooded : ";
echo $sheep->cold_blooded . "<br><br>";


// Class Frog
$kodok = new Frog("buduk");
echo "Name : ";
echo $kodok->name . "<br>"; 
echo "Legs : ";
echo $kodok->legs . "<br>";
echo "Cold Blooded : ";
echo $kodok->cold_blooded . "<br>";
echo "Jump : ";
$kodok->jump(); 


// Class APE
$sungokong = new Ape("kera sakti");
echo "Name : ";
echo $sungokong->name . "<br>"; 
echo "Legs : ";
echo $sungokong->apelegs . "<br>";
echo "Cold Blooded : ";
echo $sungokong->cold_blooded . "<br>";
echo "Yell : ";
$sungokong->yell(); 

?>
