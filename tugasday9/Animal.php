<?php
class Animal
{
    public $name;
    public $legs = 4;
    public $cold_blooded = "no";
    public $apelegs = 2;

    public function __construct($name)
    {
        $this->name = $name;
    }
}
?>
