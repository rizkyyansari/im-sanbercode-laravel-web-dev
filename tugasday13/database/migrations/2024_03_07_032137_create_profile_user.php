<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('profile_user', function (Blueprint $table) {
            $table->id('id');
            $table->integer('umur')->nullable()->default(null);
            $table->text('biodata')->nullable()->default(null);
            $table->text('alamat')->nullable()->default(null);
            $table->unsignedBigInteger('users_id')->nullable();            
            $table->timestamps();


            $table->foreign('users_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('profile_user');
    }
};
