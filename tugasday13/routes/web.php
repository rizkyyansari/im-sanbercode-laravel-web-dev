<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/welcome', function () {
    return view('pages.welcome');
});


Route::get('/', function () {
    return view('pages.data-table');    
});

Route::get('/table', function () {
    return view('pages.table');    
});

Route::get('/data-table', function () {
    return view('pages.data-table');    
});



//user routing


Route::get('/manage-user',[UserController::class,'indexUser']);



//CRUD Table Casts

//Route To Index Data Cast
Route::get('/manage-cast', [CastController::class,'indexCast']);
//Route to Form Tambah Cast
Route::get('/tambah-cast', [CastController::class,'createCast']);
//Route Store Data Cast Into Database
Route::post('/store-cast', [CastController::class,'storeCast']);
//Route To See Detail Cast
Route::get('/cast/{cast_id}',[CastController::class,'detailCast']);
//Route to Form Edit Cast
Route::get('/cast/edit/{cast_id}',[CastController::class,'editCast']);
//Route to Update Data Cast
Route::put('/cast/{cast_id}',[CastController::class,'updateCast']);
//Route to Delete Data Cast
Route::delete('/cast/{cast_id}',[CastController::class,'destroyCast']);


