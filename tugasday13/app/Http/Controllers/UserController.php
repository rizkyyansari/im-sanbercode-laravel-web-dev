<?php

namespace App\Http\Controllers;
use App\Models\User;

use Illuminate\Http\Request;

class UserController extends Controller
{
      
   public function indexUser(){
    $data = User::get();
    
    return view('pages.manage-user.indexuser', compact('data'));

   }
}