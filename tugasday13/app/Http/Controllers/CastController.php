<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class CastController extends Controller
{
      
   public function indexCast(){

    $casts = DB::table('casts')->get(); 
    
    return view('pages.manage-cast.indexcast', [ 'casts' => $casts ]);

   }

   public function createCast(){

      return view('pages.manage-cast.tambahcast');

   }

   public function storeCast(Request $request){
      $request->validate([
         'nama_cast' => 'required',
         'umur' => 'required',
         'bio' => 'required'
     ]);

     DB::table('casts')->insert([
         'nama_cast' => $request['nama_cast'],
         'umur' => $request['umur'],
         'bio' => $request['bio'],
         'created_at' => now(),
         'updated_at' => now()
     ]);
     return redirect('/manage-cast');
   }

   public function detailCast($id){
      $casts = DB::table('casts')-> where('id',$id)->first();

      return view('pages.manage-cast.detailcast', ['casts' => $casts]);
   }

   public function editCast($id){
      $casts = DB::table('casts')-> where('id',$id)->first();

      return view('pages.manage-cast.editcast', ['casts' => $casts]);
   }

   public function updateCast(Request $request, $id) {
      $request ->  validate ([
          'nama_cast'=>'required',
          'umur'=>'required|max:255',
          'bio'=>'required'
      ],[
        'nama_cast.required'=>'Nama Cast harus diisi',
        'umur.required'=>'Umur Harus Diisi',
        'umur.max'=>'Umur Maximal 255 Karakter',
        'bio.required'=>'Bio Harus Diisi'
    ]);
      DB::table('casts')
      ->where('id', $id)
      ->update([
        'nama_cast' => $request->nama_cast,
        'umur' => $request->umur,
        'bio' => $request->bio,
        'updated_at' => now()
      ]);
      return redirect('/manage-cast');
   }

   public function destroyCast($id){
      $casts = DB::table('casts')-> where('id',$id)->delete();

      return redirect('/manage-cast');
   }
}