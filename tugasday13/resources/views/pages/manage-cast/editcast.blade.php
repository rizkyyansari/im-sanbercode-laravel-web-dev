@extends('layout.master')

@section('judul')
    Edit Data Cast
@endsection

@section('content')
<div>
    <h2>Edit Cast</h2>
        <form action="/cast/{{$casts->id}}" method="POST">
            @csrf
            @method('PUT')            
            <div class="form-group">
                <label>Nama Cast</label>
                <input type="text" class="form-control" name="nama_cast" id="age" value="{{$casts->nama_cast}}">
                @error('nama_cast')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="number" class="form-control" name="umur" id="age" value="{{$casts->umur}}">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Biodata</label>
                <textarea class="form-control" name="bio" id="bio2" cols="30" rows="10">{{$casts->bio}}</textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">EDIT</button>
        </form>
@endsection