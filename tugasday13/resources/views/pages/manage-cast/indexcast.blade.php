@extends('layout.master')

@section('judul')
    Manange Cast Film
@endsection

@push('scripts')
  <script>
      $(document).ready( function () {
      $('#myTable').DataTable();
      } );
  </script>    
  <script src="https://cdn.datatables.net/v/bs4/dt-2.0.1/datatables.min.js"></script>
@endpush

@push('styles')
<link href="https://cdn.datatables.net/v/bs4/dt-2.0.1/datatables.min.css" rel="stylesheet">
@endpush

@section('content')
<a href="/tambah-cast" class="btn btn-primary" style="float: right"><i class="fas fa-plus"></i>Tambah User</a>
<table id="myTable" class="table table-bordered table-striped">
  <thead>
  <tr>
    <th>No</th>
    <th>Nama Cast</th>
    <th>Umur Cast</th>
    <th>Biodata Cast</th>
    <th>Action</th>
  </tr>
  </thead>
  <tbody>
    @forelse ($casts as $key  => $value)
  <tr>
        <td>{{$loop->iteration}}.</td>
        <td>{{$value->nama_cast}}</td>
        <td>{{$value->umur}} Tahun</td>
        <td>{{$value->bio}}</td>
        <td>
          <form action="/cast/{{$value->id}}" method="POST">
            @csrf
            @method('DELETE')
              <a href="/cast/{{$value->id}}" class="btn btn-info btn-sm">Detail</a>
              <a href="/cast/edit/{{$value->id}}" class="btn btn-primary btn-sm">Edit</a>
              <input type="submit" value="Delete" class="btn btn-danger btn-sm">              
              {{-- <a href="" class="btn btn-danger"><i class="fas fa-trash-alt"></i>hapus</a> --}}
            </form>
        </td>       
  </tr>  
    @empty
       <tr>
        <td>Data Tidak Ada</td>
       </tr>
    @endforelse 
      </tbody>
</table>
@endsection