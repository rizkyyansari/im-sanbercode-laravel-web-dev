@extends('layout.master')

@section('judul')
    Tambah Data Cast
@endsection

@section('content')
<div>
    <h2>Tambah Data Cast</h2>
        <form action="/store-cast" method="POST">
            @csrf            
            <div class="form-group">
                <label>Nama Cast</label>
                <input type="text" class="form-control" name="nama_cast" id="age" placeholder="Masukkan Umur Cast">
                @error('nama_cast')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Umur</label>
                <input type="number" class="form-control" name="umur" id="age" placeholder="Masukkan Umur Cast">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label>Biodata</label>
                <textarea class="form-control" name="bio" id="bio2" cols="30" rows="10"></textarea>
                @error('bio')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
@endsection