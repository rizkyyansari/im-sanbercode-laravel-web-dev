@extends('layout.master')

@section('judul')
    Detail Cast
@endsection


@section('content')
    <div class="form-group">
        <label>Nama Cast</label>
        <input type="text" class="form-control"  value="{{$casts->nama_cast}}" disabled>        
    </div>
    <div class="form-group">
        <label>Umur</label>
        <input type="text" class="form-control"  value="{{$casts->umur}} Tahun" disabled>        
    </div>
    <div class="form-group">
        <label>Bio</label>
        <input type="text" class="form-control"  value="{{$casts->bio}}" disabled>        
    </div>
    <a href="/manage-cast" class="btn btn-secondary">Kembali</a>
@endsection