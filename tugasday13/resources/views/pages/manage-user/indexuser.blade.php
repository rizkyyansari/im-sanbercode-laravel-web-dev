@extends('layout.master')

@section('judul')
    Manange Cast Film
@endsection

@push('scripts')
  <script>
      $(document).ready( function () {
      $('#myTable').DataTable();
      } );
  </script>    
  <script src="https://cdn.datatables.net/v/bs4/dt-2.0.1/datatables.min.js"></script>
@endpush

@push('styles')
<link href="https://cdn.datatables.net/v/bs4/dt-2.0.1/datatables.min.css" rel="stylesheet">
@endpush

@section('content')

<table id="myTable" class="table table-bordered table-striped">
  <thead>
    <a href="#" class="btn btn-primary" style="float: right"><i class="fas fa-plus"></i>Tambah User</a>
  <tr>
    <th>No</th>
    <th>Nama User</th>
    <th>Email</th>
    <th>Action</th>
  </tr>
  </thead>
  <tbody>
    @foreach ($data as $d)
  <tr>
    
        <td>{{$loop->iteration}}.</td>
        <td>{{$d->name}}</td>
        <td>{{$d->email}}</td>
        <td>
            <a href="" class="btn btn-primary"><i class="fas fa-pen"></i>Edit</a>
            <a href="" class="btn btn-danger"><i class="fas fa-trash-alt"></i>hapus</a>
        </td>
       
  </tr>  
    @endforeach 
</table>
@endsection