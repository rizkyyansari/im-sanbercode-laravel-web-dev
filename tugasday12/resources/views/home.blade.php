
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Homepage</title>
    <link href="{{ asset('home.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    
    <div class="header">
        <nav>
            {{-- navbar sec --}}
        </nav>
    </div>
    
    <div class="heading">
        <h1>SanberBook</h1>
        <h2>Social Media Developer Santai Berkualitas</h2>
        <p>Belajar Dan Berbagi agar hidup ini semakin santai berkualitas</p>
    </div>
    <div class="row">
        <div class="card-container">
            <div class="column">
                <h1>Benefit Join SanberBook</h1>
                <div class="list1">
                    <ul>
                        <li>Mendapatkan Motivasi dari sesama Developer</li>
                        <li>Sharing Knowledge dari para mentor Sanber</li>
                        <li>Dibuat oleh calon web developer terbaik</li>
                    </ul>
                </div>
            </div>
            <div class="column">
                <h1>Cara Bergabung ke SanberBook</h1>
                <div class="list2">
                    <ol>
                        <li>Mengunjungi Website ini</li>
                        <li>Mendaftar di <a href="/register">Form Sign Up</a></li>
                        <li>Selesai</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    
</body>
<style>
    
</style>
</html>