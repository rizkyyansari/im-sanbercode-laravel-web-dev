<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Register</title>
    <link href="{{ asset('register.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <h1>Buat Account Baru</h1>
    <div class="card">
        <form action="/kirim" method="POST" >
            @csrf
            <h2>Sign Up Form</h2>
            <label for="">First Name:</label><br>
            <input type="text" name="fname" id="fname"><br><br>

            <label for="">Last Name:</label><br>
            <input type="text" name="lname" id="lname"><br><br>

            <label for="gender">Gender:</label><br>
            
            <input type="radio" id="male" name="gender" value="Male">
            <label for="male">Male</label><br>
            <input type="radio" id="female" name="gender" value="Female">
            <label for="female">Female</label><br>
            <input type="radio" id="other" name="gender" value="other">
            <label for="female">other</label><br><br>

            <label for="">Nationality:</label>

            <select name="nationality" id="nationality">
                <option value="Indonesian">Indonesian</option>
                <option value="American">American</option>
                <option value="Russian">Russian</option>
            </select><br><br>


            <label for="">Language Spoken:</label><br>
            <checkbox>
                <input type="checkbox" name="language[]" id="ind" value="Indonesia"> Indonesia<br>
                <input type="checkbox" name="language[]" id="eng" value="English"> English<br>
                <input type="checkbox" name="language[]" id="arb" value="Arabic"> Arabic<br>
                <input type="checkbox" name="language[]" id="jap" value="Japanese"> Japanese<br>
            </checkbox><br>

            <label for="">Bio:</label><br>
            <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br><br>
            
              
            <button class = "button"type="submit" value="kirim">Sign Up</button>   

        </form>
    </div>
</body>


</html>