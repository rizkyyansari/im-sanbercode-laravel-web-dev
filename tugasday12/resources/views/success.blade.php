<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Success Register</title>
    <link href="{{ asset('success.css') }}" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="success">
        <h1>Selamat Datang <br>{{$fname}} {{$lname}} !</h1>
        <div class="isianform">
            <p>Gender : {{$gender}}</p>
            <p>Nationality :{{$nationality}}</p>
            <p>Language Spoken:
                @foreach($language as $lang)
                    {{$lang}}@if (!$loop->last), @endif
                @endforeach
            </p>
            <p>Bio : {{$bio}}</p>
        </div>
        <div class="message">
            <p>Terimakasih telah bergabung di Sanberbook. Social Media Kita Bersama! </p>
        </div>
        
    </div>
</body>
</html>