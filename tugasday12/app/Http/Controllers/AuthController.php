<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    function navregister (){
        return view ('register');
    }

    function registerdata (Request $request) 
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];
        
        //validate the data
        // $this->validate($request, [
        //     'fname' => ['required', 'string'],
        //     'lname' => ['required', 'string']
        // ]);

        //insert into database
        // $user = new \App\Models\User;
        // $user -> fname= $fname;
        // $user -> lname= $lname;
        // $user -> gender= $gender;
        // $user -> nationality= $nationality;
        // $user -> language= $language;
        // $user -> bio= $bio;

        return view('success', 
        [   'fname' => $fname, 
            'lname' => $lname,
            'gender' => $gender,
            'nationality' => $nationality,
            'language' =>  $language,
            'bio' =>  $bio
        ]);
    }
}
